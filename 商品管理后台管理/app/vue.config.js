module.exports = {
    publicPath: './',
    chainWebpack: config => {
        config.when(process.env.NODE_ENV === 'production', config => {
            config.entry('app').clear().add('./src/main.js')
            config.set('externals', {
                'Vue': 'Vue',
                'echarts': 'echarts',
                'ElEMENTUI-ui': 'ElEMENTUI',
            })
        config.plugin('html').tap(args => {
            args[0].isProd = true
            return args
        })
    })
    config.when(process.env.NODE_ENV === 'development', config => {
        config.entry('app').clear().add('./src/main.js')
        config.plugin('html').tap(args => {
            args[0].isProd = true
            return args
        })
    })

},
    productionSourceMap: false,
}                           