import server from './http.js'


const authList =async () =>{
    let data =await server.get('/roles')
    return data
}
const authPush =async (msg) =>{
    console.log(msg);
    let data =await server.post('/roles',{roleName:msg.roleName,roleDesc: msg.roleDesc,})
    return data
}
const authDel =async (msg) =>{
    console.log(msg);
    let data =await server.delete('/roles/'+msg.id)
    return data
}
const authUpdate =async (msg) =>{
    console.log(msg);
    let data =await server.put('roles/'+msg.id,{roleName:msg.roleName,roleDesc:msg.roleDesc})
    return data
}
//权限
const authLimits =async () =>{
    let data =await server.get('rights/tree')
    return data
}
const undatarole =async (msg) =>{
    console.log(msg);
    let data =await server.post(`roles/${msg.id}/rights`, { rids: msg.rids })
    return data
}

const delroleById = async (msg) => {
    let data = await server({url:`roles/${msg.roleId}/rights/${msg.rightId}`,method:'delete'})
    return data
}

const limits = async (msg) => {
    let data = await server.get('rights/list')
    return data
}


export {
    authList,authPush,authDel,authUpdate,authLimits,undatarole,delroleById ,limits
}