// import axios from "axios"
// // import { config } from "vue/types/umd"

// const server=axios.create({
//     baseURL: process.env.VUE_APP_API,
//     timeout: 5000
// })

// server.interceptors.request.use(config=>{
//     return config
// },error=>{
//     Promise.reject(error)
// })

// server.interceptors.response.use(res=>{
//     return res.data
// },error=>{
//     Promise.reject(error)
// })


// export default server



import axios from 'axios'
import { Message, MessageBox  } from 'element-ui' //在js文件中，需单独引入提示

const server = axios.create({
    // baseURL: 'http://127.0.0.1:8888/api/private/v1',
    
    
    baseURL: process.env.VUE_APP_API,

    timeout: 5000,
})
//请求拦截
server.interceptors.request.use(config => {
    //给请求头统一加token，不用每次进后台，每个页面都加了
    if (localStorage.getItem('token')) {
        config.headers.Authorization = localStorage.getItem('token');
    }
    // if(localStorage.getItem("token")){
    //     config.headers.Authorization = localStorage.getItem("token")
    // }
    return config
}, error => {
    console.log();
    Promise.reject(error)
})
//响应拦截

let  flag=1
server.interceptors.response.use(res => {
    //根据自己后端返回状态码，设置统一的错误提醒
//   if (res.data.meta.status !== 200) {
//     Message.error({
//       duration:2000,
//       message: res.data.meta.msg
//     })
//   }
const msg=res.data.meta.msg
// console.log(res);
const code=res.data.meta.status ||200
console.log(code);  
 
    if(msg==='无效token'){
        // setTimeout({
            
        // },3000)
     
        if(flag==1){
               MessageBox.confirm('无效token,是否重新登录', '提示', {
            confirmButtonText: '重新登录',
            // cancelButtonText: '取消',
            type: 'warning',

          }).then(() => {
           location.href="/home"
          })
        flag=2
        }
     
    }else if(code >=300&& code<200){
        Message.error(msg)
    }else if(code ===201){
        Message({
            message: '恭喜你，添加成功',
            type: 'success'
          });
    }else if(code ===200){
        Message({
            message: res.data.meta.msg,
            type: 'success'
          });
    }else if(code ===400){
        Message({
            message: res.data.meta.msg,
            type: 'success'
          });
    }
    
  return res.data

}, error => {
    console.log(error);
    let mesg= error
    if(mesg=='Error: Network Error'){
// console.log("傻逼");
Message.error('服务器链接错误')
// location.href="/home"
    }
    Promise.reject(error)
})
export default server