// import server  from "./http.js";


// const login= async (msg)=>{
//     let data= await server.post('/login',msg)
//     return data
// }

// export {
//     login
// }


import server from './http.js' //导入axios

const login = async (msg) => {
    let data = await server.post('/login', msg)
    console.log(data);
    return data
}
const userlist = async (msg) => {
    let data = await server.get('/users?' + msg)
    return data
}
const leftbat = async () => {
    let data = await server.get('/menus')
    return data
}
const adduser = async (msg) => {
    let data = await server.post('/users', { username: msg.username, password: msg.password, email: msg.email, mobile: msg.mobile })
    return data
}
const amends = async (msg) => {
    console.log(msg.id);
    let data = await server.get('/users/' + msg.id)
    return data
}
const amenemd = async (msg) => {
    console.log(msg);
    let data = await server.put('/users/' + msg.id, { email: msg.email, mobile: msg.mobile })
    return data
}
const dels = async (msg) => {
    console.log(msg);
    let data = await server.delete('/users/' + msg.id)
    return data
}
const condition = async (msg) => {
    console.log(msg);
    let data = await server.put('/users/' + msg.id +/state/ + msg.type)
    return data
}
const role = async (msg) => {
    console.log(msg);
    let data = await server.get('/roles')
    return data
}
const rperg = async (msg) => {
    console.log(msg.role);
    let data = await server.put('/users/'+msg.id+ /role/,{rid:msg.role})
    return data
}
export {
    login, userlist, leftbat, adduser, amends, amenemd,dels,condition,role,rperg
}