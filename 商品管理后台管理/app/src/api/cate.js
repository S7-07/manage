import server from './http.js'


const catelist= async (msg)=>{
    console.log(msg);
    let data = await server.get('categories',{params:msg})
    return data
}
const cateok= async (msg)=>{
    console.log(msg);
    let data = await server.put('categories/'+msg.id,{cat_name:msg.cat_name})
    return data
}
const catedel= async (msg)=>{
    console.log(msg);
    let data = await server.delete('categories/'+msg.id)
    return data
}
const catepare= async (msg)=>{
    console.log(msg);
    let data = await server.get('categories',{params:msg})
    return data
}
const cateGeng= async (msg)=>{
    console.log(msg);
    let data = await server.post('categories/',
    {cat_pid:msg.pare.cat_pid,cat_name:msg.pare.cat_name,cat_level:msg.pare.cat_level})
    return data
}
const parelist= async (msg)=>{
    console.log(msg);
    let data = await server.get('categories',{params:msg})
    return data
}
const paretree= async (msg)=>{
    console.log(msg);
    let data = await server.get(`categories/${msg.id}/attributes?sel=${msg.sel}`)
    return data
}
const pareUpset= async (msg)=>{
    console.log(msg);
    let data = await server.post(`categories/${msg.id}/attributes`,{
        attr_name:msg.attr_name,attr_sel:msg.attr_sel,attr_vals:msg.attr_vals})
    return data
}
const pareDel= async (msg)=>{
    console.log(msg);
    let data = await server.delete(`categories/${msg.id}/attributes/${msg.attrid}`)
    return data
}
const pareQuer= async (msg)=>{
    console.log(msg);
    let data = await server.get(`categories/${msg.id}/attributes/${msg.attrid}`)
    return data
}

const parePush= async (msg)=>{
    console.log(msg);
    // console.log(`categories/${msg.id}/attributes/${msg.attr_Id}`,{params:{
    //     attr_name:msg.attr_name,attr_sel:msg.attr_sel}});
    let data = await server.put(`categories/${msg.id}/attributes/${msg.attr_Id}`,{
        attr_name:msg.attr_name,attr_sel:msg.attr_sel,attr_vals:msg.attr_vals})
    return data
}

const parepull= async (msg)=>{
    console.log(msg);
    // console.log(`categories/${msg.id}/attributes/${msg.attr_Id}`,{params:{
    //     attr_name:msg.attr_name,attr_sel:msg.attr_sel}});
    let data = await server.put(`categories/${msg.id}/attributes/${msg.attr_Id}`,{
        attr_name:msg.attr_name,attr_sel:msg.attr_sel})
    return data
}
export {
    catelist,cateok,catedel,catepare,cateGeng,parelist,paretree,pareUpset,pareDel,parePush,pareQuer,parepull
}