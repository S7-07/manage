import server from './http.js'



const Goodlist = async (msg) => {
    console.log(msg);
    let data = await server.get('goods', { params: msg.list })
    return data
}


const GoodDel = async (msg) => {
    console.log(msg);
    let data = await server.delete('goods/' + msg.id)
    return data
}

const GoodCode = async (msg) => {
    console.log(msg);
    let data = await server.get('categories')
    return data
}

const Goodlet = async (msg) => {
    console.log(msg);
    let data = await server.get(`categories/${msg.id}/attributes?sel=${msg.sel}`)
    return data
}
const Goodmset = async (msg) => {
    console.log(msg);
    let data = await server.get(`categories/${msg.id}/attributes?sel=${msg.sel}`)
    return data
}
const GoodUpload = async (msg) => {
    console.log(msg);
    let data = await server.post('/goods', msg)
    return data
}

const goodOrders = async (msg) => {
    console.log(msg);
    let data = await server.get(`orders?query=${msg.query}&pagenum=${msg.pagenum}&pagesize=${msg.pagesize}`)
    return data
}

const getReports = async () => {
    return await server.get("reports/type/1")
}


export {
    getReports, Goodlist, GoodDel, GoodCode, Goodlet, Goodmset, GoodUpload, goodOrders
}