import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElEMENTUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
// import App from './App.vue';
// 导入富文本编辑器
import VueQuillEditor from 'vue-quill-editor'
// require styles
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
Vue.use(ElEMENTUI);
Vue.config.productionTip = false

import TreeTable from 'vue-table-with-tree-grid'

Vue.component('tree-table',TreeTable)

Vue.use(VueQuillEditor)
router.beforeEach((to,from,next)=>{
  if(to.path === '/home')
   return next();
   const token=localStorage.getItem("token")
 if(!token){
    // alert('您还没有登录，请先登录');
  return  next('/home');
  }
  next()
}) 



Vue.filter('filters',function(time) {
  let date = new Date(time*1000);//如果date为13位不需要乘1000
  let Y = date.getFullYear() + '-';
  let M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
  let D = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate()) + ' ';
  let h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
  let m = (date.getMinutes() <10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
  let s = (date.getSeconds() <10 ? '0' + date.getSeconds() : date.getSeconds());
  return Y+M+D+h+m+s;
})
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
