import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    name: 'Home',
    component: Home

  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue'),
    redirect: "/happy",
    children: [
      {
        path: '/users',
        name: 'users',
        component: () => import('../views/user1.vue'),
      },
      {
        path: '/happy',
        name: 'happy',
        component: () => import('../views/happy.vue'),
      },
      {
       path: '/roles',
        name: "roles" ,
        component: () => import('../views/authority2.vue'),
      },
      {
        path: '/rights',
        name: 'rights',
        component: () => import('../views/Commodity3.vue'),
      },

      {
        path: '/goods',
        name: "goods",
        component: () => import('../views/good.vue'),
      },
      {
        path: '/goods/add',
        name: "goods/add",
        component: () => import('../views/add.vue'),
      }, 
      {
        path: '/params',
        name: "params",
        component: () => import('../views/params.vue'),
      }, 
      {
        path: '/categories',
        name: "categories",
        component: () => import('../views/categories.vue'),
      },{
        path: '/orders',
        name: "orders",
        component: () => import('../views/orders.vue'),
      },{
        path: '/reports',
        name: "reports",
        component: () => import('../views/reports.vue'),
      },
    ]
  }, {
    path: '*',
    redirect: '/home',
  }
]

const router = new VueRouter({
  routes
})

// router.beforeEach(to,form,)
export default router
